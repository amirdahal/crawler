const puppeteer = require('puppeteer');
const axios = require('axios');

var urls;

var browser;
var page;

const crawl = async (url) => {
  var x;
  try {
    await page.setDefaultNavigationTimeout(10000);

    var res = await page.goto(url.url);
    var pageSource = await page.evaluate(() => document.documentElement.outerHTML);
    var status_code = res._status;
    if (status_code >= 300 && status_code < 400) {
      status_code = 200;
    }
    var content_length = res._headers['content-length'];
    if ((typeof content_length) === 'undefined') {
      content_length = Buffer.byteLength(pageSource, 'utf8');
    }
    var file_time = res._headers['date'];
    if ((typeof file_time) === 'undefined') {
      file_time = Date.now();
    }
    x = {
      entityid: url.entityid,
      entity_subid: url.entity_subid,
      http_code: status_code,
      size_download: content_length,
      filetime: file_time,
      header: JSON.stringify(res._headers),
      page: pageSource
    };
  } catch (err) {
    //console.log(err);
    x = {
      entityid: url.entityid,
      entity_subid: url.entity_subid,
      http_code: 500,
      size_download: 0,
      filetime: Date.now(),
      header: 'Source Not Accessible',
      page: err.message
    };
  }
  return x;
};


const get_urls = async () => {
  try {
    var res = await axios.post('http://1.7.151.12:8181/bidocean/api/se/request_v2.php', {
      "request": 'get_se_source',
      "api_key": 'DAASc02418Er6E712D3474DevServer58z570vE',
      "mode": "1"
    });
    urls = res.data;
    //console.log(res.data);
  } catch (err) {
    console.error(err);
  }
};

const put_se_dat = async (data) => {
  data.request = "save_json"
  data.api_key = "DAASc02418Er6E712D3474DevServer58z570vE"
  try {
    const res = await axios.post('http://1.7.151.12:8181/bidocean/api/se/request_v2.php', data);
    console.log(res.data);
  } catch (err) {
    console.error(err);
  }
};


(async () => {
  browser = await puppeteer.launch({ headless: true });
  page = await browser.newPage();
  console.log('Getting urls...');
  await get_urls();
  console.log('Crawling...');
  for (var url of urls) {
    try {
      var res = await crawl(url);
      console.log(res);
      //await put_se_dat(res);
    } catch (err) {
      console.log(err);
    }
  }

  await browser.close();
})();


